<?php

namespace Drupal\wt_linkformatter\Plugin\Field\FieldFormatter;


use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\link\Plugin\Field\FieldFormatter\LinkFormatter;
use Drupal\Component\Utility\Unicode;

/**
 * Plugin implementation of the 'link' formatter.
 *
 * @FieldFormatter(
 *   id = "wt_link",
 *   label = @Translation("Link without schema://www."),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class WtLinkFormatter extends LinkFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'remove_schema' => '',
      'remove_www' => '',
      'remove_slash' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['remove_schema'] = [
      '#type' => 'checkbox',
      '#title' => t('Render without schema'),
      '#default_value' => $this->getSetting('remove_schema'),
    ];
    $elements['remove_www'] = [
      '#type' => 'checkbox',
      '#title' => t('Render without www subdomain'),
      '#default_value' => $this->getSetting('remove_www'),
    ];
    $elements['remove_slash'] = [
      '#type' => 'checkbox',
      '#title' => t('Remove trailing slash'),
      '#default_value' => $this->getSetting('remove_slash'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    $settings = $this->getSettings();

    if (!empty($settings['remove_schema'])) {
      $summary[] = t('Render without schema');
    }
    if (!empty($settings['remove_www'])) {
      $summary[] = t('Render without www subdomain');
    }
    if (!empty($settings['remove_slash'])) {
      $summary[] = t('Render without trailing slash');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    $entity = $items->getEntity();
    $settings = $this->getSettings();

    foreach ($items as $delta => $item) {
      // By default use the full URL as the link text.
      $url = $this->buildUrl($item);
      $link_title = $url->toString();

      // If the title field value is available, use it for the link text.
      if (empty($settings['url_only']) && !empty($item->title)) {
        // Unsanitized token replacement here because the entire link title
        // gets auto-escaped during link generation in
        // \Drupal\Core\Utility\LinkGenerator::generate().
        $link_title = \Drupal::token()->replace($item->title, [$entity->getEntityTypeId() => $entity], ['clear' => TRUE]);
      }

      // Remove schema prefix
      if (!empty($settings['remove_schema'])) {
        $link_title = preg_replace('#^https?://#', '', $link_title);
      }

      // Remove www subdomain prefix
      if (!empty($settings['remove_www'])) {
        $link_title = preg_replace('#^(https?://)?www\.#', '${1}', $link_title);
      }

      // Remove trailing slash
      if (!empty($settings['remove_slash'])) {
        $link_title = rtrim($link_title,'/');
      }

      // Trim the link text to the desired length
      if (!empty($settings['trim_length'])) {
        $link_title = Unicode::truncate($link_title, $settings['trim_length'], FALSE, TRUE);
      }

      if (!empty($settings['url_only']) && !empty($settings['url_plain'])) {
        $element[$delta] = [
          '#plain_text' => $link_title,
        ];

        if (!empty($item->_attributes)) {
          // Piggyback on the metadata attributes, which will be placed in the
          // field template wrapper, and set the URL value in a content
          // attribute.
          // @todo Does RDF need a URL rather than an internal URI here?
          // @see \Drupal\Tests\rdf\Kernel\Field\LinkFieldRdfaTest.
          $content = str_replace('internal:/', '', $item->uri);
          $item->_attributes += ['content' => $content];
        }
      }
      else {
        $element[$delta] = [
          '#type' => 'link',
          '#title' => $link_title,
          '#options' => $url->getOptions(),
        ];
        $element[$delta]['#url'] = $url;

        if (!empty($item->_attributes)) {
          $element[$delta]['#options'] += ['attributes' => []];
          $element[$delta]['#options']['attributes'] += $item->_attributes;
          // Unset field item attributes since they have been included in the
          // formatter output and should not be rendered in the field template.
          unset($item->_attributes);
        }
      }
    }

    return $element;
  }
}
